using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FarClipUIToggle : MonoBehaviour
{

    [SerializeField] private InputActionAsset _inputActions;
    [SerializeField] private Canvas _wristCanvas;
    [SerializeField] private InputAction _inventory;
    [SerializeField] private bool _isReveal;
    // Start is called before the first frame update
    void Start()
    {
        _isReveal = true;
        _wristCanvas = GetComponent<Canvas>();
        _inventory = _inputActions.FindActionMap("XRI LeftHand").FindAction("Inventory");
        _inventory.Enable();
        _inventory.performed += ToggleMenu;
    }

    private void OnDisable()
    {
        _inventory.performed -= ToggleMenu;
    }
    public void ToggleMenu(InputAction.CallbackContext ctx)
    {

        if (_isReveal)
        {
            _wristCanvas.enabled = false;
            _isReveal = false;
        }

        else
        {
            _wristCanvas.enabled = true;
            _isReveal = true;
        }
    }
}
