using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarclipButton : MonoBehaviour
{
    [SerializeField] private Camera _playerCamera;

    void Start()
    {
        
    }

    // Update is called once per frame
    public void SetCameraFarCulling(float cullingDistance)
    {
        _playerCamera.farClipPlane = cullingDistance;
    }
}
