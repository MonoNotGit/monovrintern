using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject _playerObj;
    private Vector3 currentYposition;
    // Start is called before the first frame update
    void Start()
    {
        currentYposition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerObj != null)
        {
            // Get the current position of the game object

            // Get the target position (player's X and Z, but current object's Y)
            Vector3 targetPosition = new Vector3(_playerObj.gameObject.transform.position.x, currentYposition.y, _playerObj.gameObject.transform.position.z);

            // Set the game object's position to the target position
            transform.position = targetPosition;
        }
    }
}
