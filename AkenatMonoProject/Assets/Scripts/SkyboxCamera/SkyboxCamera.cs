using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxCamera : MonoBehaviour
{
    [SerializeField] private Transform playerCamera;
    private Vector3 currentBGCamera;
    [SerializeField] private float skyboxScale;

    private void Start()
    {
        currentBGCamera = transform.position;
    }

    private void Update()
    {
        transform.rotation = playerCamera.rotation;

        Vector3 targetPosition = new Vector3(playerCamera.position.x, 1.76f,playerCamera.position.z);
        transform.localPosition = targetPosition / skyboxScale;
    }

}
