using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.XR.Interaction.Toolkit;

public class PimParticleCube : MonoBehaviour
{
    [SerializeField] private bool _isPrime;
    [SerializeField] private bool _isExpanded;
    [SerializeField] private float _defaultSize;
    [SerializeField] private Color _defaultColor;



    private XRGrabInteractable grabInteractable;
    private bool isReleased;

    // Start is called before the first frame update
    void Start()
    {
        _defaultSize = this.transform.localScale.x;
        _defaultColor = this.GetComponent<MeshRenderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Collider>() != null && _isPrime && !_isExpanded && IsReleased())
        {
            this.transform.DOScale(1.15f, 0.56f).SetEase(Ease.InElastic);
            _isExpanded = true;
        }
    }
    public void PrimeCube()
    {
        _isPrime = true;

        Material objMats = this.GetComponent<MeshRenderer>().material;

        objMats.DOColor(Color.green, 0.15f);

        Debug.Log("Prime");
    }

    public void ResetCube()
    {
        if(_isPrime && _isExpanded)
        {
            _isPrime = false;
            _isExpanded = false;

            Material objMats = this.GetComponent<MeshRenderer>().material;
            objMats.DOColor(_defaultColor, 0.15f);
            this.transform.DOScale(_defaultSize, 0.2f).SetEase(Ease.OutBounce);
        }
    }




    void Awake()
    {
        // Get the XRGrabInteractable component
        grabInteractable = GetComponent<XRGrabInteractable>();

        // Register event handlers
        grabInteractable.selectExited.AddListener(OnSelectExited);
    }

    void OnDestroy()
    {
        // Unregister event handlers to avoid memory leaks
        grabInteractable.selectExited.RemoveListener(OnSelectExited);
    }

    public void EnterHand(SelectEnterEventArgs args)
    {
        isReleased = false;
        ResetCube();
    }

    public void OnSelectExited(SelectExitEventArgs args)
    {
        // Set the flag to true when the object is released
        isReleased = true;
        Debug.Log("Object released from hand.");
    }

    public bool IsReleased()
    {
        return isReleased;
    }
}
