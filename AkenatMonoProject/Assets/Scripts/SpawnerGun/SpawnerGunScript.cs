using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerGunScript : MonoBehaviour
{
    [SerializeField] private SlotForSpawner _slotForSpawnerRef;
    [SerializeField] private GameObject _objectToSpawn;
    [SerializeField] private LayerMask _raycastLayerMask;
    [SerializeField] private float _raycastDistance;
    [SerializeField] private Transform _spawnPoint;
    private Vector3 _hitPoint;
    [SerializeField] private LineRenderer _gunLaser;
    private GameObject _storeSpawnedObject;

    [Header("Laser")]
    [SerializeField] private Color _defaultColor;
    [SerializeField] private Color _hitColor;
    [SerializeField] private Color _noHitColor;
    [SerializeField] private Color _noObjectColor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HandleRaycast();
    }



    public void HandleRaycast()
    {

            if (Physics.Raycast(_spawnPoint.position, _spawnPoint.transform.forward, out RaycastHit hit, _raycastDistance, _raycastLayerMask))
            {
                // Check if the raycast hit an object

                // Store the hit position in the public variable
                _hitPoint = hit.point;

                // Update the LineRenderer to show the ray hitting the object
                _gunLaser.SetPosition(0, transform.position);
                _gunLaser.SetPosition(1, hit.point);
                _gunLaser.startColor = _hitColor;
                _gunLaser.endColor = _hitColor;
            }
            else
            {
                // No object hit by the raycast


                // Set the hit position to a default value (e.g., Vector3.zero) if no hit
                _hitPoint = Vector3.zero;

                // Update the LineRenderer to show the ray going to the maximum distance

                _gunLaser.SetPosition(0, transform.position);
                _gunLaser.SetPosition(1, transform.position + transform.forward * _raycastDistance);
                _gunLaser.startColor = _noHitColor;

            }
            _gunLaser.endColor = _noHitColor;
        
    }


    public void SpawnObjectHere()
    {
        if (_hitPoint != Vector3.zero)
        {
            if (_slotForSpawnerRef.CurrenSpawnerItem != null)
            {
                if (_storeSpawnedObject != null)
                {
                    Destroy(_storeSpawnedObject);
                    _storeSpawnedObject = Instantiate(_slotForSpawnerRef.CurrenSpawnerItem, _hitPoint, Quaternion.identity);
                    _storeSpawnedObject.GetComponent<Rigidbody>().useGravity = true;
                }

                else
                {
                    _storeSpawnedObject = Instantiate(_slotForSpawnerRef.CurrenSpawnerItem, _hitPoint, Quaternion.identity);
                    _storeSpawnedObject.GetComponent<Rigidbody>().useGravity = true;
                }



            }

            else
            {
                // no item man;
            }
            
        }

        else
        {
            //cant spawn man
        }
    }
}
