using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class InventoryManager : MonoBehaviour
{
    private static InventoryManager _inventoryInstance;
    public static InventoryManager InventoryInstance;

    [SerializeField] private bool _acceptPlacement;
    public bool AcceptPlacement => _acceptPlacement;

    [SerializeField] private List<GameObject> _wristInventory = new List<GameObject>();
    [SerializeField] private List<GameObject> _invenSlots = new List<GameObject>();

    private void Awake()
    {
        if (InventoryInstance == null)
        {
            InventoryInstance = this;
        }
        else if (InventoryInstance != this)
        {
            Destroy(gameObject);
        }
    }



    public void StoreInventoryItem(GameObject itemToStore)
    {

        _wristInventory.Add(itemToStore);
    }

    public void RemoveInventoryItem(GameObject itemToRemove)
    {
        if(_wristInventory.Contains(itemToRemove))
        {
            _wristInventory.Remove(itemToRemove);
        }
    }

    public void HideItemList()
    {
        _acceptPlacement = false;

        if (_wristInventory.Count != 0)
        {
            List<GameObject> inventoryItems = new List<GameObject>(_wristInventory);

            foreach (GameObject inventoryItem in inventoryItems)
            {
                inventoryItem.GetComponent<MeshRenderer>().enabled = false;
                inventoryItem.GetComponent<BoxCollider>().enabled = false;
            }

        }

        foreach (GameObject slots in _invenSlots)
        {
           
        }

       


    }

    public void ShowItemList()
    {

        _acceptPlacement = true;

        if (_wristInventory.Count != 0)
        {
            foreach (GameObject inventoryItem in _wristInventory)
            {
                inventoryItem.SetActive(true);
                inventoryItem.GetComponent<BoxCollider>().enabled = true;
                inventoryItem.GetComponent<MeshRenderer>().enabled = true;
                //inventoryItem.GetComponent<MeshRenderer>().enabled = true;
            }


        }

       

    }
}
