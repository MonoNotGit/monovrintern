using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SlotTest : MonoBehaviour
{
     XRSocketInteractor socket;
    [SerializeField] private GameObject _gameObjInThisSlot;


    void Start()
    {
        socket = GetComponent<XRSocketInteractor>();


        if (socket != null)
        {
            socket.selectEntered.AddListener(OnSelectEntered);
            socket.selectExited.AddListener(OnSelectExited);
        }
    }
    public void DebugLogPlease()
    {
        Debug.Log("InventorySlot1");
    }



    private void OnDestroy()
    {
        if (socket != null)
        {
            socket.selectEntered.RemoveListener(OnSelectEntered);
            socket.selectExited.RemoveListener(OnSelectExited);
        }
    }

    public void OnSelectEntered(SelectEnterEventArgs args)
    {
        //if(InventoryManager.InventoryInstance.AcceptPlacement)
        //{
        //    GameObject enteredObject = args.interactableObject.transform.gameObject;
        //    InventoryManager.InventoryInstance.StoreInventoryItem(enteredObject);
        //}

        GameObject enteredObject = args.interactableObject.transform.gameObject;
        InventoryManager.InventoryInstance.StoreInventoryItem(enteredObject);

        //Debug.Log($"Object Entered: {enteredObject.name}");
        // You can add more logic here to handle the object entering the socket
    }

    public void OnSelectExited(SelectExitEventArgs args)
    {
        //if (InventoryManager.InventoryInstance.AcceptPlacement)
        //{
        //    GameObject exitedObject = args.interactableObject.transform.gameObject;
        //    InventoryManager.InventoryInstance.StoreInventoryItem(exitedObject);
        //}


        GameObject exitedObject = args.interactableObject.transform.gameObject;
        InventoryManager.InventoryInstance.RemoveInventoryItem(exitedObject);
        //Debug.Log($"Object Exited: {exitedObject.name}");
        // You can add more logic here to handle the object exiting the socket
    }

    public void ItemToSlot()
    {

        IXRSelectInteractable socketedObj = socket.GetOldestInteractableSelected();

        GameObject refSocketObj = socketedObj.transform.gameObject;

        InventoryManager.InventoryInstance.StoreInventoryItem(refSocketObj);



    }

    public void RemoveItemToWorld()
    {
        IXRSelectInteractable socketedObj = socket.GetOldestInteractableSelected();

        GameObject refSocketObj = socketedObj.transform.gameObject;

        InventoryManager.InventoryInstance.RemoveInventoryItem(refSocketObj);

    }
}



