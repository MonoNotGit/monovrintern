using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HandAnimationController : MonoBehaviour
{
    [SerializeField] private InputActionReference _gripInputActionRef;
    [SerializeField] private InputActionReference _triggerInputActionRef;

    [Header("Animator")]
    [SerializeField] private Animator _handAnimator;
    [SerializeField] private float _gripValue;
    [SerializeField] private float _triggerValue;
    // Start is called before the first frame update
    void Start()
    {
        _handAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleGrip();
        HandleTrigger();
    }

    public void HandleGrip()
    {
        _gripValue = _gripInputActionRef.action.ReadValue<float>();
        _handAnimator.SetFloat("Grip", _gripValue);

    }

    public void HandleTrigger()
    {
            _triggerValue = _triggerInputActionRef.action.ReadValue<float>();
            _handAnimator.SetFloat("Pinch", _triggerValue);

    }
}
