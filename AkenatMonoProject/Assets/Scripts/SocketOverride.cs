using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Filtering;

public class SocketOverride : MonoBehaviour, IXRHoverFilter
{
    

    public bool canProcess => isActiveAndEnabled;

    //public bool Process(IXRSelectInteractor interactor, IXRSelectInteractable interactable)
    //{
    //    bool canSelect = InventoryManager.InventoryInstance.AcceptPlacement;

    //    return canSelect;
        

    //}

    public bool Process(IXRHoverInteractor interactor, IXRHoverInteractable interactable)
    {
        bool canHover = InventoryManager.InventoryInstance.AcceptPlacement;

        return canHover;
    }
}
